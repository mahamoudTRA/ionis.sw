﻿using IONIS.SW.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IONIS.SW.Test
{
    class Program
    {
        static string MenuConnexion()
        {
            Console.WriteLine("BIENVENUE SUR BRAIN SMART");
            Console.WriteLine("************************************************************************");
            Console.WriteLine("1. Connectez vous");
            Console.WriteLine("2. Inscrivez vous");
            Console.WriteLine("3. Quittez");
            Console.WriteLine("Votre choix");
            string choix = Console.ReadLine();
            return choix;
        }

        static string MenuChefDeProjet()
        {
            Console.WriteLine("BIENVENUE SUR BRAIN SMART");
            Console.WriteLine("************************************************************************");
            Console.WriteLine("1. Creer un nouveau brainstorming");
            Console.WriteLine("2. Participez a un brainstorming");
            Console.WriteLine("3. Consulter mes brainstorming");
           // Console.WriteLine("4. Retour");
            string choix = Console.ReadLine();
            return choix;
        }

        static void MenuParticipant()
        {
            Console.WriteLine("BIENVENUE SUR BRAIN SMART");
            Console.WriteLine("************************************************************************");
            Console.WriteLine("1. Participez a un brainstorming");
            Console.WriteLine("2. Consulter mes brainstorming");
          //  Console.WriteLine("3. Retour");
            
        }


        static void ShowMyBrainstorming(string con, PersonData currentPerson)
        {
           
            OpinionGateway opinionGateway = new OpinionGateway(con);
            List<IdeaOpinionData> opinionIdeas;
           
            MeetingGateway meetingGateway = new MeetingGateway(con);
            IEnumerable<MeetingData> meetings = meetingGateway.FindMeeting(currentPerson.PersonId).Result;
            //IdeaData idea;
            Console.WriteLine("Liste de mes brainstorming");
            meetings = meetingGateway.FindMeeting(currentPerson.PersonId).Result;
            if (meetings.ToList().Count > 0)
            {
                foreach (var item in meetings)
                {
                    Console.WriteLine("ID: {0} - Titre: {1}, - Description: {2}", item.MeetingId, item.Title, item.Description);
                }
                Console.WriteLine("Choisir 1 brainstorming et voir les idees proposee");
                int choix = int.Parse(Console.ReadLine());
                opinionIdeas = opinionGateway.GetBestIdeaMeeting(choix).Result.ToList();
                if (opinionIdeas.Count == 0) Console.WriteLine("Aucune idee proposee pour ce brainstorming pour l'instant");
                else
                {
                    int i = 1;
                    foreach (var item in opinionIdeas)
                    {
                        Console.WriteLine("Idee {0}: <<{1}>> ", i, item.Idea);
                        i++;
                        
                    }
                    IdeaOpinionData idea = opinionIdeas.Where(ii => ii.Nombre == opinionIdeas.Max(j => j.Nombre) && ii.Avis.Equals("J'aime")).FirstOrDefault();
                    Console.WriteLine("IDEE Commune: <<{0}>> a eu {1} mention {2}", idea.Idea, idea.Nombre,idea.Avis);
                }

            }


        }


        static void ParticipateToBrainstorming(string con, PersonData currentPerson)
        {
            int choix4;
            OpinionData opinion;
            OpinionGateway opinionGateway = new OpinionGateway(con);
            List<IdeaData> ideas;
            MeetingData currentMeeting;
            MeetingGateway meetingGateway = new MeetingGateway(con);
            IEnumerable<MeetingData> meetings = meetingGateway.FindMeeting(currentPerson.PersonId).Result;
            foreach (var item in meetings)
            {
                Console.WriteLine("ID: {0} - Titre: {1} - Description: {2}, - Date Reunion: {3}",
                    item.MeetingId, item.Title, item.Description, item.HoldingDate);
            }

            do
            {
                choix4 = int.Parse(Console.ReadLine());
                currentMeeting = meetingGateway.FindById(choix4).Result.Content;
            } while (currentMeeting == null);

            do
            {
                Console.WriteLine("1. Proposer une idee");
                Console.WriteLine("2. Donnez votre avis");
                Console.WriteLine("Votre choix:");
                choix4 = int.Parse(Console.ReadLine());
                if (choix4 == 1 || choix4 == 2) break;

            } while (true);
            IdeaData idea = new IdeaData();
            if (choix4 == 1)
            {

                ideas = new List<IdeaData>();
                do
                {
                    Console.WriteLine("Proposer votre idee OU -fin pour QUITTER");
                    idea.Wording = Console.ReadLine();
                    if (idea.Wording.Equals("fin")) break;
                    idea.MeetingId = currentMeeting.MeetingId;
                    idea.PersonId = currentPerson.PersonId;
                    idea.CreationDate = DateTime.Now;
                    ideas.Add(idea);
                    var res = opinionGateway.CreateIdea(idea.Wording, currentMeeting.MeetingId, idea.PersonId, idea.CreationDate);
                } while (true);
            }

            if (choix4 == 2)
            {
                Console.WriteLine("Votre avis par aux idees suivantes");
                ideas = opinionGateway.FindMeetingIdea(currentMeeting.MeetingId).Result.ToList();
                ideas = ideas.Where(ii => ii.PersonId != currentPerson.PersonId).ToList();
                foreach (var item in ideas)
                {
                    Console.WriteLine("ID: {0} - Idee: {1}", item.IdeaId, item.Wording);
                }
                Console.WriteLine("Choisissez une idee");
                int choix1 = int.Parse(Console.ReadLine());
                idea = ideas.Where(i => i.IdeaId == choix1).FirstOrDefault();

                if (idea != null)
                {
                    Console.WriteLine("Votre avis par rapport a l'idee: <<{0}>>", idea.Wording);
                    Console.WriteLine("1. J'aime");
                    Console.WriteLine("2. J'aime pas");
                    Console.WriteLine("3. Sans avis");
                    Console.WriteLine("Votre choix: ");
                    int choix2 = int.Parse(Console.ReadLine());

                    switch (choix2)
                    {
                        case 1:
                            opinion = new OpinionData();
                            opinion.IdeaId = idea.IdeaId;
                            opinion.Type = "J'aime";
                            opinion.PersonId = currentPerson.PersonId;
                            var res = opinionGateway.AddOpinion(opinion.PersonId, opinion.IdeaId, opinion.Type);
                            break;
                        case 2:
                            opinion = new OpinionData();
                            opinion.IdeaId = idea.IdeaId;
                            opinion.Type = "J'aime pas";
                            opinion.PersonId = currentPerson.PersonId;
                            _ = opinionGateway.AddOpinion(opinion.PersonId, opinion.IdeaId, opinion.Type);
                            break;
                        case 3:
                            opinion = new OpinionData();
                            opinion.IdeaId = idea.IdeaId;
                            opinion.Type = "Sans avis";
                            opinion.PersonId = currentPerson.PersonId;
                            _ = opinionGateway.AddOpinion(opinion.PersonId, opinion.IdeaId, opinion.Type);
                            break;
                    }
                }
            }

        }

        static void CreateBrainstorming(string con, PersonData currentPerson)
        {
            int choix2 = int.Parse(MenuChefDeProjet());
            MeetingGateway meetingGateway = new MeetingGateway(con);
            MeetingData currentMeeting;
            IEnumerable<PersonData> persons;
            PersonGateway personGateway = new PersonGateway(con);
            List<ParticipateData> participates;
            persons = personGateway.GetAll().Result;
            switch (choix2)
            {
                case 1:

                    Console.WriteLine("Donnez le titre");
                    string title = Console.ReadLine();
                    Console.WriteLine("Donnez la description");
                    string desc = Console.ReadLine();
                    Console.WriteLine("Donnez la date de la reunion");
                    DateTime meetingDate = Convert.ToDateTime(Console.ReadLine());
                    var res = meetingGateway.CreateMeeting(title, desc, DateTime.Now, meetingDate, currentPerson.PersonId);
                    currentMeeting = meetingGateway.GetLast().Result.Content;
                    if (currentMeeting != null)
                    {
                        foreach (var item in persons)
                        {
                            Console.WriteLine("ID : {0} - Prenom : {1} - Nom: {2} - Mail: {3} - Profil: {4}",
                                item.PersonId, item.FirstName, item.LastName, item.Mail, item.Profil);
                        }
                        participates = new List<ParticipateData>();
                        PersonData personAdd;
                        int choix3;
                        ParticipateData p;
                        do
                        {
                            Console.WriteLine("Donnez l'ID de la personne a ajouter et -1 pour QUITTER");
                            choix3 = int.Parse(Console.ReadLine());
                            if (choix3 != -1)
                            {
                                p = participates.Where(pp => pp.PersonCreateId == choix3).FirstOrDefault();
                                if (p == null)
                                {
                                    personAdd = persons.Where(pp => pp.PersonId == choix3).FirstOrDefault();
                                    if (personAdd != null)
                                    {
                                        p = new ParticipateData();
                                        p.MeetingId = currentMeeting.MeetingId;
                                        p.PersonCreatorId = currentPerson.PersonId;
                                        p.PersonCreateId = personAdd.PersonId;
                                        participates.Add(p);
                                    }
                                    else
                                    {
                                        Console.WriteLine("Cette personne n'existe pas dans la base de donnees");
                                    }
                                }
                                else
                                {
                                    Console.WriteLine("Cette personne a deja ete ajoutee dans cette reunion");
                                }
                            }
                        } while (choix3 != -1);
                        foreach (var item in participates)
                        {
                            _ = meetingGateway.AddParticipant2(item.PersonCreatorId, item.MeetingId, item.PersonCreateId);
                        }
                    }
                    break;
                case 2:
                    ParticipateToBrainstorming(con, currentPerson);
                    break;
                case 3:

                    ShowMyBrainstorming(con, currentPerson);

                    break;
            }
        }
        static void Main(string[] args)
        {
            string con = "Server =.\\SQLEXPRESS; Database = sw; Trusted_Connection = True;MultipleActiveResultSets=true";

            
            PersonData currentPerson;
           
            
            IEnumerable<PersonData> persons;
           
            List<string> profils = new List<string>() { "Chef", "Participant" };

            string email;
            string password;

            OpinionData opinion = new OpinionData();

            PersonGateway personGateway = new PersonGateway(con);
            OpinionGateway opinionGateway = new OpinionGateway(con);
            MeetingGateway meetingGateway = new MeetingGateway(con);
            persons = personGateway.GetAll().Result;

            
            int choix1 = int.Parse(MenuConnexion());
            

            //persons = new IEnumerable<PersonData>();
            
        

            switch(choix1)
            {
                case 1:

                    do
                    {
                    
                        Console.WriteLine("Votre mail de connexion");
                        email = Console.ReadLine();
                        password = Console.ReadLine();
                        Console.WriteLine("Votre mot de passe");
                        password = Console.ReadLine();
                        currentPerson = new PersonData();
                        currentPerson = personGateway.FindPersonByMail(email, password).Result.Content;
                        if(currentPerson == null)
                        {
                            Console.WriteLine("Vos identifiants sont incorrects");
                        }
                    } while (currentPerson == null);

                    Console.WriteLine("BIENVENUE {0} {1}", currentPerson.FirstName, currentPerson.LastName);
                    if (currentPerson.Profil.Equals("Chef"))
                    {
                        CreateBrainstorming(con, currentPerson);     
                    }
                    else
                    {
                        MenuParticipant();
                        int chx = int.Parse(Console.ReadLine());
                        switch(chx)
                        {
                            case 1:
                                ParticipateToBrainstorming(con, currentPerson);
                                break;
                            case 2:
                                ShowMyBrainstorming(con, currentPerson);
                                break;
                        }
                    }
                    break;
                case 2:
                    int i = 1;
                    int choix;
                    Console.WriteLine("Creation Nouveau compte");
                    PersonData person = new PersonData();
                    Console.WriteLine("Votre Nom :");
                    person.LastName = Console.ReadLine();
                    Console.WriteLine("Votre Prenom :");
                    person.FirstName = Console.ReadLine();
                    Console.WriteLine("Votre Mail :");
                    person.Mail = Console.ReadLine();
                    Console.WriteLine("Votre Mot de passe :");
                    person.Password = Console.ReadLine();
                    Console.WriteLine("Selectionnez un profil");
                    foreach (var item in profils)
                    {
                        Console.WriteLine("{0}. {1}", i, item);
                        i++;
                    }
                  
                        choix = int.Parse(Console.ReadLine());
                        
                    if (choix == 1) person.Profil = "Chef";
                    else person.Profil = "Participant";
                    var res = personGateway.CreatePerson(person.Mail, person.Password, person.FirstName, person.LastName, person.Profil);
                   
                    break;
                case 3:
                    Console.WriteLine("A tres bientot");
                    break;
                default:
                    break;
            }

            Console.WriteLine("Vous Trouverez le code : https://gitlab.com/mahamoudTRA/ionis.sw.git !");
            Console.ReadKey();
        }
    }
}
