﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IONIS.SW.WebApp.Models
{
    public class OpinionModel
    {
        public int OpinionId { get; set; }

        public string Type { get; set; }

        public int PersonId { get; set; }
        public int IdeaId { get; set; }
    }
}
