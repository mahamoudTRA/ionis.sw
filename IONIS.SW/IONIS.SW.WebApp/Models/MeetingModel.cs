﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IONIS.SW.WebApp.Models
{
    public class MeetingModel
    {
        public int MeetingId { get; set; }

        public string Title { get; set; }
        public string Description { get; set; }


        public DateTime CreationDate { get; set; }

        public DateTime HoldingDate { get; set; }

        public int Statut { get; set; }

        public int PersonId { get; set; }
    }
}
