﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IONIS.SW.WebApp.Models
{
    public class IdeaModel
    {
        public int IdeaId { get; set; }

        public string Wording { get; set; }

        public int PersonId { get; set; }
        public int MeetingId { get; set; }

        public DateTime CreationDate { get; set; }
    }
}
