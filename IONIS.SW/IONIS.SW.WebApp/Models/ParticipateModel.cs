﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IONIS.SW.WebApp.Models
{
    public class ParticipateModel
    {
        public int ParticipateId { get; set; }
        public int PersonCreatorId { get; set; }
        public int PersonCreateId { get; set; }
        public int MeetingId { get; set; }
    }
}
