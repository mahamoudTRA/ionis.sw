﻿using IONIS.SW.DAL;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IONIS.SW.WebApp.Controllers
{
    //[Route("Meeting")]
    public class MeetingController : Controller
    {
        readonly MeetingGateway _meetingGateway;

        public MeetingController(MeetingGateway meetingGateway)
        {
            _meetingGateway = meetingGateway;
        }
        [HttpPost]
        public async Task<IActionResult> Create(string title, string description, DateTime creationDate, DateTime holdingDate, int personId)
        {
            var res = await _meetingGateway.CreateMeeting(title, description, creationDate, holdingDate, personId);
            return View();
        }
    }
}
