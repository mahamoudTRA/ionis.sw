﻿using System;

namespace IONIS.SW.DAL
{
    public class PersonData
    {
        public int PersonId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Mail { get; set; }

        public string Password { get; set; }

        public string Profil { get; set; }
    }
}
