﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IONIS.SW.DAL
{
    public class OpinionData
    {
        public int OpinionId { get; set; }

        public string Type { get; set; }

        public int PersonId { get; set; }
        public int IdeaId { get; set; }
       
    }
}
