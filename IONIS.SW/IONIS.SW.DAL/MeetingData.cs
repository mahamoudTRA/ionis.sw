﻿using System;

namespace IONIS.SW.DAL
{
    public class MeetingData
    {
        public int MeetingId { get; set; }

        public string Title { get; set; }
        public string Description { get; set; }

   
        public DateTime CreationDate { get; set; }

        public DateTime HoldingDate { get; set; }

        public int Statut { get; set; }

        public int PersonId { get; set; }
    }
}
