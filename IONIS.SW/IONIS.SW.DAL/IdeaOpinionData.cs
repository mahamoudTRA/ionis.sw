﻿using System;

namespace IONIS.SW.DAL
{
    public class IdeaOpinionData
    {
        public int Nombre { get; set; }

        public string Avis { get; set; }
        public string Idea { get; set; }
    }
}
