﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;

namespace IONIS.SW.DAL
{
    public class MeetingGateway
    {
        readonly string _connectionString;

        public MeetingGateway(string connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<Result<MeetingData>> FindById(int Id)
        {
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                MeetingData meeting = await con.QueryFirstOrDefaultAsync<MeetingData>(
                    @"select m.MeetingId,m.Title,m.[description],m.PersonId,
                             m.CreationDate, m.HoldingDate from 
                             ionis.vMeeting  m where m.MeetingId = @MeetingId",
                    new { MeetingId = Id });
                if (meeting == null) return Result.Failure<MeetingData>(Status.NotFound, "Meeting not found.");
                return Result.Success(meeting);
            }
        }

        public async Task<IEnumerable<MeetingData>> FindMeeting(int Id)
        {
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                IEnumerable<MeetingData> meetings = await con.QueryAsync<MeetingData>(
                    @"select distinct(m.Title), m.MeetingId,m.Title,m.[Description],
                             m.PersonId, m.CreationDate,m.HoldingDate
			                from ionis.tParticipate p1, ionis.tPerson p2, ionis.tMeeting m 
			          where (p1.PersonCreateId = p2.PersonId 
                             or p2.PersonId = p1.PersonCreatorId) 
                             and p2.PersonId = @PersonId ",
                    new { PersonId = Id });
                return meetings;
            }
        }

        public async Task<Result<MeetingData>> GetLast()
        {
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                MeetingData meeting = await con.QueryFirstOrDefaultAsync<MeetingData>(
                    @"select m.MeetingId,m.Title,m.[description],m.PersonId,
                             m.CreationDate, m.HoldingDate from 
                             ionis.vMeeting  m where 
                        m.MeetingId = (SELECT MAX(MeetingId) FROM ionis.vMeeting)");
                if (meeting == null) return Result.Failure<MeetingData>(Status.NotFound, "Any meeting is DATABASE.");
                return Result.Success(meeting);
            }
        }

        public async Task<IEnumerable<MeetingData>> GetAll()
        {
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                return await con.QueryAsync<MeetingData>(
                    @"select m.MeetingId,m.Title,m.[description],m.PersonId,
                             m.CreationDate, m.HoldingDate from 
                             ionis.vMeeting m");
                    
            }
        }


        public async Task<IEnumerable<PersonData>> FindParticipant(int Id)
        {
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                return await con.QueryAsync<PersonData>(
                    @"select m.MeetingId,m.Title,m.description,m.PersonId,
                             m.CreationDate, m.HoldingDate,Lastname, 
                             Firstname, mail, profil from 
                    ionis.vMeetingParticipant m where m.MeetingId = @MeetingId",
                    new { MeetingId = Id });
            }
        }


        public async Task<Result<int>> CreateMeeting(string title,string description, DateTime creationDate, DateTime holdingDate, int personId)
        {
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                var p = new DynamicParameters();
                p.Add("@Title", title);
                p.Add("@Description", description);
                p.Add("@CreationDate", creationDate);
                p.Add("@HoldingDate", holdingDate);
                p.Add("@Statut", 0);
                p.Add("@PersonId", personId);
                p.Add("@MeetingId", dbType: DbType.Int32, direction: ParameterDirection.Output);
                p.Add("@Status", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                await con.ExecuteAsync("ionis.sMeetingCreate", p, commandType: CommandType.StoredProcedure);

                int status = p.Get<int>("@Status");
                if (status == 1) return Result.Failure<int>(Status.BadRequest, "An Meeting with this title already exists.");

                Debug.Assert(status == 0);
                return Result.Success(p.Get<int>("@Meeting"));
            }
        }

        public async Task<Result<int>> AddParticipant(int ParticipantId,int meetingId, DateTime meetingDate)
        {
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand();

                var p = new DynamicParameters();

                p.Add("@MeetingId", meetingId);
                p.Add("@ParticipantId", ParticipantId);
                p.Add("@MeetingDate", meetingDate);
                p.Add("@ParticipateId", dbType: DbType.Int32, direction: ParameterDirection.Output);
                p.Add("@Status", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                await con.ExecuteAsync("ionis.sParticipateCreate", p, commandType: CommandType.StoredProcedure);

                int status = p.Get<int>("@Status");
                if (status == 1) return Result.Failure<int>(Status.BadRequest, "An Meeting with this participate already exists.");

                Debug.Assert(status == 0);
                return Result.Success(p.Get<int>("@ParticipateId"));
            }
        }

        public async Task<int> AddParticipant2(int creator, int meetingId, int create)
        {
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                await con.OpenAsync();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                string req = @"INSERT INTO ionis.tParticipate(MeetingId,PersonCreatorId, PersonCreateId)
                                VALUES(@meeting, @creator, @create)";
                cmd.CommandText = req;
                cmd.Parameters.AddWithValue("@meeting", meetingId);
                cmd.Parameters.AddWithValue("@creator", creator);
                cmd.Parameters.AddWithValue("@create", create);
                return cmd.ExecuteNonQueryAsync().Result;
            }
        }



    }
}
