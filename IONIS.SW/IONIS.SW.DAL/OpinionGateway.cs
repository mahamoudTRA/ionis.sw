﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;

namespace IONIS.SW.DAL
{
    public class OpinionGateway
    {
        readonly string _connectionString;

        public OpinionGateway(string connectionString)
        {
            _connectionString = connectionString;
        }

        

        public async Task<IEnumerable<PersonData>> FindParticipant(int Id)
        {
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                return await con.QueryAsync<PersonData>(
                    @"select m.FirstName, m.LastName, m.Mail from 
                    ionis.vMeetingParticipant m where m.MeetingId = @MeetingId",
                    new { MeetingId = Id });       
            }
        }

        public async Task<IEnumerable<IdeaData>> FindMeetingIdea(int Id)
        {
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                return await con.QueryAsync<IdeaData>(
                    @"select i.MeetingId, i.IdeaId, i.Wording from 
                    ionis.vMeetingIdea i where i.MeetingId = @MeetingId",
                    new { MeetingId = Id });
            }
        }

        public async Task<IEnumerable<IdeaOpinionData>> GetBestIdeaMeeting(int Id)
        {
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                IEnumerable<IdeaOpinionData> idea = await con.QueryAsync<IdeaOpinionData>(
                    @"select Nombre = COUNT(o.[Type]), Idea = i.Wording, Avis = o.[Type] from
                            ionis.tIdea i inner join ionis.tOpinion o 
		                on o.IdeaId = i.IdeaId where i.MeetingId = @MeetingId group by i.Wording,o.[Type]",
                    new { MeetingId = Id });
                return idea;
            }
        }


        public async Task<IEnumerable<IdeaData>> FindMeetingIdeaWithOpinion(int Id)
        {
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                IEnumerable<IdeaData> ideas = new List<IdeaData>();
                await con.OpenAsync();
                SqlCommand cmd = new SqlCommand();
                SqlCommand cmd2 = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandText = @"SELECT m.MeetingId, i.IdeaId, i.Wording, i.PersonId from ionis.tIdea i
                                    inner join ionis.tMeeting m on m.MeetingId = i.MeetingId WHERE i.MeetingId = @Id";
                cmd.Parameters.AddWithValue("@id", Id);
                cmd2.Connection = con;
                SqlDataReader dr = await cmd.ExecuteReaderAsync();
                while(await dr.ReadAsync())
                {
                    IdeaData idea = new IdeaData();
                    idea.IdeaId = dr.GetInt32(1);
                    idea.Wording = dr.GetString(2);
                    idea.PersonId = dr.GetInt32(3);
                    idea.MeetingId = dr.GetInt32(0);
                   
                    
                    cmd2.CommandText = @"SELECT * FROM ionis.tOpinion o inner join ionis.tIdea i on i.IdeaId = o.IdeaId AND i.IdeaId = @Id";
                    cmd2.Parameters.AddWithValue("Id", idea.IdeaId);
                    SqlDataReader dr2 = await cmd2.ExecuteReaderAsync();
                    while(await dr2.NextResultAsync())
                    {
                        OpinionData opinion = new OpinionData();
                        opinion.OpinionId = dr2.GetInt32(0);
                        opinion.Type = dr2.GetString(1);
                        opinion.PersonId = dr2.GetInt32(2);
                        opinion.IdeaId = dr2.GetInt32(3);
                        idea.Opinions.Add(opinion);
                    }
                    ideas.AsList().Add(idea);
                }
                return ideas;
            }
        }

        public async Task<Result<int>> CreateIdea(string wording,int meetingId, int personId, DateTime creationDate)
        {
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                var p = new DynamicParameters();
                p.Add("@Wording", wording);
                p.Add("@MeetingId", meetingId);
                p.Add("@CreationDate", creationDate);
                p.Add("@PersonId", personId);
                p.Add("@IdeaId", dbType: DbType.Int32, direction: ParameterDirection.Output);
                p.Add("@Status", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                await con.ExecuteAsync("ionis.sIdeaCreate", p, commandType: CommandType.StoredProcedure);

                int status = p.Get<int>("@Status");
                if (status == 1) return Result.Failure<int>(Status.BadRequest, "An Opinion with this comment already exists.");

                Debug.Assert(status == 0);
                return Result.Success(p.Get<int>("@IdeaId"));
            }
        }

        public async Task<Result<int>> VoteCreate(int meetingId, int participantId, DateTime voteDate)
        {
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                var p = new DynamicParameters();
                p.Add("@MeetingId", meetingId);
                p.Add("@CreationDate", voteDate);
                p.Add("@ParticipantId", participantId);
                p.Add("@VoteNumber", 0);
                p.Add("@VoteId", dbType: DbType.Int32, direction: ParameterDirection.Output);
                p.Add("@Status", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                await con.ExecuteAsync("ionis.sVoteCreate", p, commandType: CommandType.StoredProcedure);

                int status = p.Get<int>("@Status");
                if (status == 1) return Result.Failure<int>(Status.BadRequest, "A vote with this user and meeting already exist.");

                Debug.Assert(status == 0);
                return Result.Success(p.Get<int>("@VoteId"));
            }
        }

        public async Task<int> AddOpinion(int person, int idea, string type)
        {
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                await con.OpenAsync();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                string req = @"INSERT INTO ionis.tOpinion(Type,PersonId, IdeaId)
                                VALUES(@type, @person, @idea)";
                cmd.CommandText = req;
                cmd.Parameters.AddWithValue("@type", type);
                cmd.Parameters.AddWithValue("@person", person);
                cmd.Parameters.AddWithValue("@idea", idea);
                return cmd.ExecuteNonQueryAsync().Result;
            }
        }

        public async Task OpinionUpate(int opinionId,string type)
        {
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                await con.ExecuteAsync(
                    "ionis.sOpiniondUpdate",
                    new { OpinionId = opinionId, Type = type },
                    commandType: CommandType.StoredProcedure);
            }
        }

    }
}
