﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IONIS.SW.DAL
{
    public class IdeaData
    {
        public int IdeaId { get; set; }

        public string Wording { get; set; }

        public int PersonId { get; set; }
        public int MeetingId { get; set; }

        public DateTime CreationDate { get; set; }

        public ICollection<OpinionData> Opinions;

        public IdeaData()
        {
            Opinions = new List<OpinionData>();
        }
    }
}
