﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;

namespace IONIS.SW.DAL
{
    public class PersonGateway
    {
        readonly string _connectionString;

        public PersonGateway(string connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<Result<PersonData>> FindPersonByMail(string mail, string password)
        {
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                PersonData user = await con.QueryFirstOrDefaultAsync<PersonData>(
                    @"select u.PersonId, u.FirstName, u.LastName, u.Mail, u.Password,u.Profil from 
                    ionis.vUser u where u.Mail = @Mail and u.Password = @Password",
                    new { Mail = mail,
                          Password = password
                        });
                if (user == null) return Result.Failure<PersonData>(Status.NotFound, "User not found.");
                return Result.Success(user);
            }
        }


        public async Task<Result<PersonData>> FindPersonById(int Id)
        {
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                PersonData person = await con.QueryFirstOrDefaultAsync<PersonData>(
                    @"select u.PersonId, u.FirstName, u.LastName, u.Mail, u.Password,u.Profil from 
                    ionis.vUser u where u.PersonId = @PersonId",
                    new { PersonId = Id });
                if (person == null) return Result.Failure<PersonData>(Status.NotFound, "User not found.");
                return Result.Success(person);
            }
        }

        public async Task<IEnumerable<PersonData>> GetAll()
        {
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                return await con.QueryAsync<PersonData>(
                    @"select u.PersonId, u.FirstName, u.LastName, u.Mail, u.Password,u.Profil from 
                    ionis.vUser u");
            }
        }

        public async Task<PersonData> GetLast()
        {
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                return await con.QueryFirstOrDefaultAsync<PersonData>(
                    @"select u.PersonId, u.FirstName, u.LastName, u.Mail, u.Password,u.Profil from 
                    ionis.vUser u WHERE u.PersonId = (SELECT MAX(PersonId) from ionis.tPerson)");
            }
        }

        public async Task<Result<int>> CreatePerson(string mail, string password, string firstname, string lastname, string profil)
        {
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                var p = new DynamicParameters();
                p.Add("@FirstName", firstname);
                p.Add("@LastName", lastname);
                p.Add("@Profil", profil);
                p.Add("@Mail", mail);
                p.Add("@Password", password);
                p.Add("@PersonId", dbType: DbType.Int32, direction: ParameterDirection.Output);
                p.Add("@Status", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                await con.ExecuteAsync("ionis.sPersonCreate", p, commandType: CommandType.StoredProcedure);

                int status = p.Get<int>("@Status");
                if (status == 1) return Result.Failure<int>(Status.BadRequest, "A person with this email already exists.");

                Debug.Assert(status == 0);
                return Result.Success(p.Get<int>("@PersonId"));
            }
        }
    }
}
