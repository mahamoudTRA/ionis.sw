create schema ionis
go
if not exists(select * from sys.databases d where d.[name] = 'sw')
begin
	create database sw;
end;
GO

use sw;
GO

if exists(select *
          from sys.tables t
			inner join sys.schemas s on s.[schema_id] = t.[schema_id]
          where t.[name] = 'tProjectCEO' and s.[name] = 'ionis')
begin
	drop table ionis.tProjectCEO;
end;

create table ionis.tProjectCEO
(
	CEOId int identity(0, 1),
	FirstName nvarchar(32) not null,
	LastName  nvarchar(32) not null,
	Mail nvarchar(32) not null,
	[Password] nvarchar(32) not null,

	constraint PK_PS_tProjectCEO primary key(CEOId),
	constraint UK_PS_tProjectCEO_Identifier unique(Mail, [Password]),

);

if exists(select *
          from sys.tables t
			inner join sys.schemas s on s.[schema_id] = t.[schema_id]
          where t.[name] = 'tParticipant' and s.[name] = 'ionis')
begin
	drop table ionis.tParticipant;
end;

create table ionis.tParticipant
(
	ParticipantId int identity(0, 1),
	FirstName nvarchar(32) not null,
	LastName  nvarchar(32) not null,
	Mail nvarchar(32) not null,
	[Password] nvarchar(32) not null,

	constraint PK_PS_tParticipant primary key(ParticipantId),
	constraint UK_PS_tParticiapnt_Identifier unique(Mail, [Password]),

);

if exists(select *
          from sys.tables t
			inner join sys.schemas s on s.[schema_id] = t.[schema_id]
          where t.[name] = 'Meeting' and s.[name] = 'ionis')
begin
	drop table ionis.tMeeting;
end;

create table ionis.tMeeting
(
	MeetingId int identity(0, 1),
	Wording nvarchar(32) not null,
	CreationDate  datetime2 not null,
	HoldingDate datetime2 not null,
	
	constraint PK_PS_tMeeting primary key(MeetingId),
	constraint UK_PS_tMeeting_Wording unique(Wording)
);

if exists(select *
          from sys.tables t
			inner join sys.schemas s on s.[schema_id] = t.[schema_id]
          where t.[name] = 'Opinion' and s.[name] = 'ionis')
begin
	drop table ionis.tOpinion;
end;

create table ionis.tOpinion
(
	OpinionId int identity(0, 1),
	Comment nvarchar(1000) not null,
	CreationDate  datetime2 not null,
	ParticipantId int not null,
	MeetingId int not null,
	VoteNumber int,
	constraint PK_SW_tOpinion primary key(OpinionId),
	constraint FK_SW_tOpinion_Participant foreign key(ParticipantId) references tParticipant(ParticipantId),
	constraint FK_SW_tOpinion_Meeting foreign key(MeetingId) references tMeeting(Meeting)
);

if exists(select *
          from sys.tables t
			inner join sys.schemas s on s.[schema_id] = t.[schema_id]
          where t.[name] = 'Participate' and s.[name] = 'ionis')
begin
	drop table ionis.tParticipate;
end;

create table ionis.tParticipate
(
	ParticipantId int identity(0, 1),
	MeetingId nvarchar(1000) not null,
	MeetingDate  datetime2 not null,
	constraint PK_SW_tOpinion primary key(ParticipantId,MeetingId,MeetingDate)
);

