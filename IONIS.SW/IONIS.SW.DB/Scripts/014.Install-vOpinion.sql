﻿create view ionis.vOpinion
as
    select
        OpinionId = o.OpinionId,
        [Type] = o.[Type],
        PersonId = o.PersonId,
        IdeaId = o.IdeaId
    from ionis.tOpinion o;
        