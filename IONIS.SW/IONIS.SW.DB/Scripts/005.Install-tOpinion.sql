﻿create table ionis.tOpinion
(
	OpinionId int identity(0, 1),
	[Type] nvarchar(15) not null,
	PersonId int,
	IdeaId int,
	--VoteNumber int,
	constraint PK_SW_tOpinion primary key(OpinionId),
	constraint FK_SW_tOpinion_Participant foreign key(PersonId) references ionis.tPerson(PersonId),
	constraint FK_SW_tOpinion_Idea foreign key(IdeaId) references ionis.tIdea(IdeaId)
);
