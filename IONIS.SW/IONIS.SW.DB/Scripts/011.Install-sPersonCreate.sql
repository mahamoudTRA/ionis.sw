﻿create proc ionis.sPersonCreate
(
    @PersonId int out,
	@FirstName nvarchar(32),
	@LastName  nvarchar(32),
	@Mail nvarchar(32) ,
	@Password nvarchar(32),
	@Profil nvarchar(10)
)
as
begin
	set transaction isolation level serializable;
	begin tran;

	if exists(select * from ionis.tPerson p where p.Mail = @Mail)
	begin
		rollback;
		return 1;
	end;

    insert into ionis.tPerson(FirstName,LastName,Mail, [Password], Profil)
	values(@FirstName,@LastName,@Mail, @Password, @Profil);
	set @PersonId = scope_identity();
	commit;
    return 0;
end;