﻿create view ionis.vMeeting
as
    select
        MeetingId = m.MeetingId,
        Title = m.Title,
		[Description] = m.[Description],
        CreationDate = m.creationDate,
        HoldingDate = m.HoldingDate,
		Statut = m.Statut,
		PersonId = m.PersonId
    from ionis.tMeeting m;
        