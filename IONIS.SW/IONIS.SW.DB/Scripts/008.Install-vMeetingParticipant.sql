﻿create view ionis.vMeetingParticipant
as
    select
        MeetingId = m.MeetingId,
        Title = m.Title,
		[Description] = m.[Description],
        CreationDate = m.creationDate,
        HoldingDate = m.HoldingDate,
		Statut = m.Statut,
		Creator = m.PersonId,
		PersonId = p2.PersonId,
		FirstName = p2.FirstName,
		LastName = p2.LastName,
		Mail = p2.Mail
		--MeetingDate = p1.MeetingDate
    from ionis.tMeeting m 
	inner join ionis.tParticipate p1 on p1.MeetingId = m.MeetingId
	inner join ionis.tPerson p2 on p2.PersonId = p1.PersonCreateId;
        
        