﻿create table ionis.tPerson
(
	PersonId int identity(0, 1),
	FirstName nvarchar(32) not null,
	LastName  nvarchar(32) not null,
	Mail nvarchar(32) not null,
	[Password] nvarchar(32) not null,
	Profil nvarchar(10),

	constraint PK_PS_tPerson primary key(PersonId),
	constraint UK_PS_tPerson_Identifier unique(Mail, [Password]),

);