﻿create proc ionis.sOpinionUpdate
(
    @OpinionId int,
	@Type nvarchar(15)
)
as
begin
	set transaction isolation level serializable;
	begin tran;

	if not exists(select * from ionis.tOpinion o where o.OpinionId = @OpinionId)
	begin
		rollback;
		return 1;
	end;

    update ionis.tOpinion set [Type] = @Type where OpinionId = @OpinionId;

	commit;
    return 0;
end;