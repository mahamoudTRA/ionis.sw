﻿create proc ionis.sOpinionCreate
(
    @OpinionId int out,
	@Type nvarchar(20),
	@PersonId int,
	@IdeaId int
)
as
begin
	set transaction isolation level serializable;
	begin tran;

	if exists(select * from ionis.tOpinion o where o.OpinionId = @OpinionId)
	begin
		rollback;
		return 1;
	end;

    insert into ionis.tOpinion([Type], PersonId, IdeaId)
	values(@Type, @PersonId, @IdeaId);
	set @OpinionId = scope_identity();
	commit;
    return 0;
end;