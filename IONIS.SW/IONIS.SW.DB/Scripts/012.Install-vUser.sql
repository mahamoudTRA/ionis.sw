﻿create view ionis.vUser
as
    select
        PersonId = p.PersonId,
        FirstName = p.FirstName,
        LastName = p.LastName,
        Mail = p.Mail,
		[Password] = p.[Password],
		Profil = p.Profil
    from ionis.tPerson p;
        