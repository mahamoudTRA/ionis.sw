﻿create table ionis.tParticipate
(
	ParticipateId int identity(0,1),
	PersonCreatorId int,
	PersonCreateId int,
	MeetingId int,
	--MeetingDate  datetime2,
	constraint PK_SW_tParticipate primary key (ParticipateId),
	constraint FK_SW_tPerson foreign key (PersonCreatorId) references ionis.tPerson(PersonId), 
	foreign key (MeetingId) references ionis.tMeeting(MeetingId),
	foreign key (PersonCreateId) references ionis.tPerson(PersonId)
);

