﻿create proc ionis.sMeetingCreate
(
	@Title nvarchar(20),
    @Description nvarchar(1000),
    @CreationDate datetime2,
	@HoldingDate datetime2,
	@Statut int,
	@PersonId int,
	@MeetingId int out
)
as
begin
	set transaction isolation level serializable;
	begin tran;

	if exists(select * from ionis.tMeeting m where m.Title = @Title)
	begin
		rollback;
		return 1;
	end;

    insert into ionis.tMeeting(Title,[Description],CreationDate,HoldingDate,Statut,PersonId)
	values(@Title,@Description,@CreationDate,@HoldingDate,@Statut,@PersonId);
	set @MeetingId = scope_identity();
	commit;
    return 0;
end;