﻿create view ionis.vMeetingIdea
as
    select
        MeetingId = m.MeetingId,
		Title = m.Title,
		[Description] = m.[Description],
		HoldingDate = m.HoldingDate,
		Statut = m.Statut,
		PersonCreatorMeeting = m.PersonId,
        MeetingCreation = m.creationDate,
		IdeaId = o.IdeaId,
		Wording = o.Wording,
		PersonCreatorIdea = o.PersonId
    from ionis.tMeeting m 
		 inner join ionis.tIdea o ON o.MeetingId = m.MeetingId;
        