﻿create table ionis.tIdea
(
	IdeaId int identity(0, 1),
	Wording nvarchar(32) not null,
	CreationDate  datetime2 not null,
	MeetingId int,
	PersonId int,
	
	constraint PK_PS_tIdea primary key(IdeaId),
	--constraint UK_PS_tMeeting_Wording unique(Wording),
	constraint FK_PS_tPerson_PersonId2 foreign key(PersonId) references ionis.tPerson(PersonId),
	constraint FK_PS_tPerson_Meeting2 foreign key(MeetingId) references ionis.tMeeting(MeetingId)
);