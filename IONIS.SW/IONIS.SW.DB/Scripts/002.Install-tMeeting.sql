﻿create table ionis.tMeeting
(
	MeetingId int identity(0, 1),
	[Description] nvarchar(1000) not null,
	Title nvarchar(20) not null,
	CreationDate  datetime2 not null,
	HoldingDate datetime2 not null,
	Statut int,
	PersonId int,
	
	constraint PK_PS_tMeeting primary key(MeetingId),
	constraint UK_PS_tMeeting_Title unique(Title),
	constraint FK_PS_tPerson_PersonId foreign key(PersonId) references ionis.tPerson(PersonId)
);