﻿create proc ionis.sParticipateCreate
(
	@PersonCreatorId int,
	@PersonCreateId int,
	@MeetingId int,
	@ParticipateId int out
)
as
begin
	set transaction isolation level serializable;
	begin tran;

	if exists(select * from ionis.tParticipate p where p.ParticipateId = @ParticipateId)
	begin
		rollback;
		return 1;
	end;

    insert into ionis.tParticipate(PersonCreatorId, PersonCreateId, MeetingId)
	values(@PersonCreatorId, @PersonCreateId, @MeetingId);
	set @ParticipateId = scope_identity();
	commit;
    return 0;
end;