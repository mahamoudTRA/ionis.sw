﻿create proc ionis.sIdeaCreate
(
	@IdeaId int out,
	@Wording nvarchar(1000),
    @PersonId int,
    @MeetingId   int,
	@CreationDate datetime2
)
as
begin
    set transaction isolation level serializable;
	begin tran;

	if exists(select * from ionis.tIdea i where i.Wording = @Wording)
	begin
		rollback;
		return 1;
	end;

    insert into ionis.tIdea(Wording,PersonId, MeetingId, CreationDate)
	values(@Wording,@PersonId, @MeetingId, @CreationDate);
	set @IdeaId = scope_identity();
	commit;
    return 0;
end;